## Aim
- This personal project is to help me learn how to do Test Driven Development.
- The app helps the user document and look after their pet dog.

## Stack
- Rendering: React, CSS
- Testing: Karma, Mocha, Chai, Sinon
- Database: Firebase
- Build tooks: Webpack & Babel
- Linting: Eslint, extension of airBnB's styleguide
- Registry: NPM (The karma package has a bug when using Yarn)
- Version control: Git

## Initialisation
- Install: ```npm i```
- Local development: ```npm start```
- Testing: ```npm test```

## Author
- Owen Greenaway
