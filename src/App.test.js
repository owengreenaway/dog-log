import React from 'react';
import { shallow, mount, render } from 'enzyme';
import App from './App';

describe('App', () => {

  describe('should render', () => {
    it("without crashing", () => {
      const wrapper = shallow(<App />);
      const length = wrapper.length;

      expect(length).to.equal(1);
    });

    it("exactly one <Reminders />", () => {
      const wrapper = shallow(<App />);
      const reminders = wrapper.find("Reminders");
      const length = reminders.length;

      expect(length).to.equal(1);
    });

    it("exactly one container", () => {
      const wrapper = shallow(<App />);
      const container = wrapper.find(".container");
      const length = container.length;

      expect(length).to.equal(1);
    });
  });

});
