import React, { Component } from 'react';
import Reminders from './reminders/Reminders';
import './App.css';

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {

  //   };
  // }
  render() {
    const fakeData = {
      reminders: [
        {
          id: 1,
          title: "Go to the vets",
          history: [],
        },
        {
          id: 2,
          title: "Worming tablets",
          history: [1496569762510],
        },
      ],
    };
    return (
      <div className="app">
        <div className="container">
          <Reminders items={fakeData.reminders} />
        </div>
      </div>
    );
  }
}

export default App;
