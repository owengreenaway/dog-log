import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Reminders from './Reminders';

const data = [
  {
    id: 1,
    title: "Go to the vets",
    history: [],
  },
  {
    id: 2,
    title: "Worming tablets",
    history: [1496569762510],
  },
];

describe('Reminders', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Reminders />);
  });

  // Rendering
  //===========================================================================
  describe('should render', () => {
    it("without crashing", () => {      
      const length = wrapper.length;
      expect(length).to.equal(1);
    });

    it("no Reminder components if items is empty", () => {
      wrapper = shallow(<Reminders items={[]}/>);
      const length = wrapper.find("Reminder").length;
      expect(length).to.equal(0);
    });

    it("two Reminder components if props.items.length = 2", () => {      
      wrapper = shallow(<Reminders items={data}/>);
      const length = wrapper.find("Reminder").length;
      expect(length).to.equal(2);
    });

  });

  // Integration
  //===========================================================================
  describe('(integration)', () => {
    it("should pass props to Reminder components", () => {
      wrapper = mount(<Reminders items={data}/>);
      const reminder = wrapper.find("Reminder").first();
      const reminderPropTitle = reminder.props().title;
      expect(reminderPropTitle).to.equal(data[0].title);
    })
  });
  
});