import React, { Component } from 'react';
import propTypes from 'prop-types';
// import './App.css';

class Reminder extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {

  //   };
  // }
  render() {
    const { title, history } = this.props;
    return (
      <div className="reminder">
        <p className="reminder__title">{title}</p>
        <p className="reminder__history">{history[0]}</p>
      </div>
    );
  }
}

Reminder.propTypes = {
  title: propTypes.string.isRequired,
  history: propTypes.arrayOf(propTypes.number).isRequired,
};

export default Reminder;
