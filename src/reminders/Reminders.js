import React, { Component } from 'react';
import propTypes from 'prop-types';
import Reminder from "./Reminder";
// import './App.css';

class Reminders extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {

  //   };
  // }
  render() {
    const { items } = this.props;
    return (
      <div className="reminders">
        { items.map(item => (
          <Reminder
            key={item.id}
            title={item.title}
            history={item.history}
          />
        ))}
      </div>
    );
  }
}

Reminders.propTypes = {
  items: propTypes.arrayOf(propTypes.object),
};

Reminders.defaultProps = {
  items: [],
};

export default Reminders;
