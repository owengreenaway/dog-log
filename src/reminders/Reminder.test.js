import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Reminder from './Reminder';

describe('Reminder', () => {
  let wrapper;  
  const defaultTitle = "The default title";
  const defaultHistory = [123,321];

  beforeEach(() => {
    const defaultProps = [
      {
        title: "Go to the vets",
        history: [123],
      }
    ];
    wrapper = shallow(<Reminder title={defaultTitle} history={defaultHistory}/>);
  });

  // Rendering
  //===========================================================================
  describe('should render', () => {
    it("without crashing", () => {      
      const length = wrapper.length;
      expect(length).to.equal(1);
    });

    it('the title based on the "title" prop', () => {
      const title = wrapper.find(".reminder__title").first().text();
      expect(title).to.equal(defaultTitle);
    });

    it('the history based on the "history" prop', () => {
      const history = wrapper.find(".reminder__history").first().text();
      expect(history).to.equal("123");
    });

  });

});